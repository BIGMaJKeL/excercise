import HomePage from '../../pages/homePage';
/**
 * As a user I want the ability to filter properties based on the number of bedrooms and bathrooms.
 */
describe('As a ANY user visiting [vacation rent page]', () => {
  beforeEach(() => {
    cy.visit('/hub-test-vacation-rentals');
  });

  it('I should be able to open vacation links', async () => {
    HomePage.vacationRentals().click();
  });

  it('I should be able to open vacation links', async () => {
    HomePage.hotTubPropertiess().click();
  });

  it.skip('The Filters selection should allow the user to select the number of either bedrooms and/or bathrooms.');
  it.skip('The selection should limit the value to an integer with a lower value of 0 (zero).');
  it.skip('The Clear Filters button should reset both filters to their lower value.');
  it.skip('The View Results button should close the Filter Results page and display properties on the hub meeting the criteria.');
});
