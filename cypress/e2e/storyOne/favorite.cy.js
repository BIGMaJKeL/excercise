/**
 * As a user I want the ability to save properties I’m looking at so they can be reviewed at a later time.
 */
describe('As a ANY user visiting [vacation rent page]', () => {
  beforeEach(() => {
    cy.visit('/hub-test-vacation-rentals');
  });

  it.skip('Each property on the hub should have an easy Favorite on/off button indicating that this property has been saved.');
  it.skip('The hub should have an indicator on the number of properties selected as saved.');
  it.skip('This indicator should show the total count of saved properties.');
  it.skip('When the indicator is clicked the hub should only display saved properties.');
  it.skip('I can un-save a property from the filtered view.');
  it.skip('When a property is selected; there should be an indicator showing the property has been saved.');
  it.skip('This indicator can be toggled on or off from the property detail’s view');
  it.skip('This change (saved/un-saved) should reflect correctly on the total saved count on the main hub');
});
