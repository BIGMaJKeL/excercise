module.exports = {
  preset: 'jest-puppeteer',
  setupFilesAfterEnv: ['./initial.setup.js'],
  globalSetup: './global.setup.js',
  testSequencer: './testSequencer.js',
  bail: false,
  cache: false,
  roots: ['../web'],
  maxWorkers: 1,
  testTimeout: 160000,
  verbose: true,
  testMatch: ['<rootDir>../web/tests/**/?(*.)+(test).js'],
  displayName: {
    name: 'SUPERCONTROL | WEB',
    color: 'magenta',
  },
  testEnvironment: './reporter/jest-html-reporter/environment.js',
  reporters: [
    'default',
    [
      'jest-html-reporters',
      {
        publicPath: './reports/web',
        filename: 'index.html',
        pageTitle: 'SUPERCONTROL | E2E Test Report | WEB',
        styleOverridePath:
          './config/reporter/jest-html-reporter/defaultTheme.css',
        logo: './docs/images/logo.png',
        includeConsoleLog: true,
        includeFailureMsg: true,
        includeSuiteFailure: true,
      },
    ],
  ],
};
