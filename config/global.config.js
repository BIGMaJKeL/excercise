const BASE_URL = 'https://www.rezfusionhubdemo.com';
const API_URL = 'https://www.rezfusionhubdemo.com/api/1';

const JUNIT_PATH = './reports/junit';
const SCREENSHOT_PATH = './reports/screenshots';

export const baseUrl = process.env.BASE_URL ? process.env.BASE_URL : BASE_URL;
export const apiUrl = process.env.API_URL ? process.env.API_URL : API_URL;
export const junitPath = process.env.JUNIT_PATH
  ? process.env.JUNIT_PATH
  : JUNIT_PATH;
export const screenshotPath = process.env.SCREENSHOT_PATH
  ? process.env.SCREENSHOT_PATH
  : SCREENSHOT_PATH;
export const consoleLogs = process.env.CONSOLE_LOGS === 'true';

export const defaultTimeout = 5000;
export const longTimeout = 15000;
export const veryLongTimeout = 30000;
export const shortTimeout = 1000;
export const veryShortTimeout = 100;
