import { setup as setupPuppeteer } from 'jest-environment-puppeteer';
import figlet from 'figlet';

export default async function globalSetup(globalConfig) {
  await setupPuppeteer(globalConfig);

  console.info('Global configuration ...');

  console.info('DONE!');

  figlet('findbu.gs', (err, data) => {
    if (err) {
      console.log('Something went wrong...');
      console.dir(err);
      return;
    }
    console.log(data);
  });
}
