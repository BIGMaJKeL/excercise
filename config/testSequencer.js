const NUMBER_OF_EXECUTIONS = process.env.NUMBER_OF_EXECUTIONS
  ? process.env.NUMBER_OF_EXECUTIONS
  : 1;
const Sequencer = require('@jest/test-sequencer').default;
class CustomSequencer extends Sequencer {
  sort(tests) {
    const initialTestsList = Array.from(tests).sort((testA, testB) =>
      testA.path > testB.path ? 1 : -1,
    );
    let newTestsList = [];
    for (let i = 0; i < NUMBER_OF_EXECUTIONS; i++) {
      newTestsList = newTestsList.concat(initialTestsList);
    }
    return newTestsList;
  }
}

module.exports = CustomSequencer;
