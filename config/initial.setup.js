import Viewport from '../constant/viewport';

global.beforeAll(async () => {
  await global.browser.createIncognitoBrowserContext();
  await global.page.setViewport(Viewport.desktop);
});
