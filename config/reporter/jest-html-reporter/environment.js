const PuppeteerEnvironment = require('jest-environment-puppeteer');
const { addAttach } = require('jest-html-reporters/helper');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');
const path = require('path');
const loggers = require('../../../utils/loggers');
const {
  captureBrowserConsoleLogs,
  screenshotPath,
} = require('../../global.config');
/**
 * @tutorial https://github.com/smooth-code/jest-puppeteer#extend-puppeteerenvironment
 */
class E2EEnvironment extends PuppeteerEnvironment {
  constructor(configuration, context) {
    super(configuration, context);
    this.testPath = context.testPath;
  }

  async setup() {
    await super.setup();
    await fs.promises.mkdir(screenshotPath, { recursive: true });
    this.global.logFileName = `web.${path.basename(this.testPath, '.js')}`;
    this.logger = loggers(this.global.logFileName);

    if (captureBrowserConsoleLogs) {
      this.global.page
        .on('console', (msg) => {
          const type = msg.type().substr(0, 3).toUpperCase();
          const logOutput = `[BROWSER] [${type}]: ${msg.text()}`;
          switch (type) {
            case 'LOG':
              this.logger.info(logOutput);
              break;
            case 'ERR':
              this.logger.error(logOutput);
              break;
            case 'WAR':
              this.logger.warn(logOutput);
              break;
            case 'INF':
              this.logger.info(logOutput);
              break;
            default:
              this.logger.info(logOutput);
              break;
          }
        })
        .on('response', (response) => {
          if (response.status() > 399) {
            this.logger.warn(
              `[BROWSER] [RESPONSE]: ${response.status()} ${response.url()}`,
            );
          }
        })
        .on('requestfailed', (request) =>
          this.logger.error(
            `[BROWSER] [REQUEST FAILED]: ${
              request.failure().errorText
            } ${request.url()}`,
          ),
        );
    }
  }

  async teardown() {
    await super.teardown();
  }

  async handleTestEvent(event, state) {
    if (event.name === 'test_fn_failure') {
      const data = await this.global.page.screenshot();

      await addAttach(data, 'Full Page Screenshot', this.global);
    }
  }
}

module.exports = E2EEnvironment;
