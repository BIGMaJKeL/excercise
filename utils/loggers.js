const winston = require('winston');
const { junitPath, fileLogs, consoleLogs } = require('../config/global.config');

const loggers = (logFileName) =>
  winston.createLogger({
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.timestamp(),
    ),
    transports: [
      new winston.transports.File({
        level: 'info',
        filename: `${junitPath}/${logFileName}.log`,
        prepend: true,
        format: winston.format.combine(
          winston.format.printf(
            (info) => `${info.timestamp} info: ${info.message}`,
          ),
        ),
        silent: !fileLogs,
      }),
      new winston.transports.File({
        level: 'error',
        filename: `${junitPath}/${logFileName}.log`,
        prepend: true,
        format: winston.format.combine(
          winston.format.printf(
            (error) => `${error.timestamp} error: ${error.message}`,
          ),
        ),
        silent: !fileLogs,
      }),
      new winston.transports.File({
        level: 'warn',
        filename: `${junitPath}/${logFileName}.log`,
        prepend: true,
        format: winston.format.combine(
          winston.format.printf(
            (warning) => `${warning.timestamp} warning: ${warning.message}`,
          ),
        ),
        silent: !fileLogs,
      }),
      new winston.transports.Console({
        stderrLevels: ['error', 'info', 'warn'],
        format: winston.format.simple(),
        silent: !consoleLogs,
      }),
    ],
  });

module.exports = loggers;
