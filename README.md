# Excercise

[rez-qa-chanllenge](https://github.com/PropertyBrands/rez-qa-challeng)

As a QA Engineer we are expected to write automation for features completed by the development team. It is our task to automate both stories One and Two, ensuring we have the proper test coverage before releasing this product.

## QA Directives:

Use Cypress.io at the latest version
https://www.cypress.io/
Use CSS to select elements (no XPATH)
Each story should have their own test suite
Automated tests should be committed to a public repo
The branch name should be your --challenge.
You can use either, your personal account, or create a ‘burner’ account for this commit.
Anyone accessing your branch should have sufficient documentation to run both suites.
Do not include the node_modules in your commit.
Visual Studio Code is the preferred IDE and can be downloaded for free
https://code.visualstudio.com/Download
You are free to commit any IDE settings you think are relevant to other engineers running your tests

### Story One:

As a user I want the ability to save properties I’m looking at so they can be reviewed at a later time.

**Acceptance Criteria:**

Each property on the hub should have an easy Favorite on/off button indicating that this property has been saved.
The hub should have an indicator on the number of properties selected as saved.
This indicator should show the total count of saved properties.
When the indicator is clicked the hub should only display saved properties.
I can un-save a property from the filtered view.
When a property is selected; there should be an indicator showing the property has been saved.
This indicator can be toggled on or off from the property detail’s view
This change (saved/un-saved) should reflect correctly on the total saved count on the main hub
Story Artifacts:

Main Hub is at https://www.rezfusionhubdemo.com/hub-test-vacation-rentals

### Story Two:

As a user I want the ability to filter properties based on the number of bedrooms and bathrooms.

**Acceptance Criteria:**

The Filters selection should allow the user to select the number of either bedrooms and/or bathrooms.
The selection should limit the value to an integer with a lower value of 0 (zero).
The Clear Filters button should reset both filters to their lower value.
The View Results button should close the Filter Results page and display properties on the hub meeting the criteria.
Story Artifacts:

Main Hub is at https://www.rezfusionhubdemo.com/hub-test-vacation-rentals

# How to run

## Dependencies

- Node version is required to be the same as is defined under the engine in [package.json](package.json)](package.json) file. Recommendation is to use [NVM](https://github.com/nvm-sh/nvm) as a manager for node version.
Once the node version is installed please follow the below instruction from the root folder

- Clone repo

```
    git clone https://bitbucket.org/BIGMaJKeL/excercise.git
```

- Install dependencies

```
    npm ci
```

## Run tests

### Puppeteer

- Run all tests

```
    npm run test:web
```
![report](Docs/img/testResults.png)

- Run a specific test suite

```
    npm run test:web:filename <full or partial name>
    e.g.
    npm run test:web:filename filters
    or
    npm run test:web:filename filters.test.js
```

### Cyress

- Run a cypress test

```
    npm run cytest:web:chrome
```

- Run a cypress test in watch mode
```
    npm run cytest:web:watch
```

- Run a cypress test for specific file name
```
    npm run cytest:web:filename 'cypress/e2e/storyTwo/filters.cy.js' 
    or
    npm run cytest:web:filename '**/filters.cy.js'
```

## Open report

The setup is for Linux but you can open report on any browser.
```
Docs
│   README.md
│
└───reports
│   │
│   └───web
│       │ index.html <----- **Open on any browser**
│
└───web
│   │
│   └───components
│   │
│   └───pages
│   │
│   └───tests
│       │
│       └───favorites
│           │ favorities.test.js
```

- Open from CLI
```
    npm run open:report:web
```
![report](Docs/img/report.png)
