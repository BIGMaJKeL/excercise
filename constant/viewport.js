const Viewport = {
  desktop: {
    width: 1920,
    height: 1080,
    isMobile: false,
  },
  // iPhone 11
  mobile: {
    width: 375,
    height: 812,
    isMobile: false,
  },
};

export default Viewport;
