module.exports = {
  launch: {
    headless: process.env.HEADLESS === 'true',
    devtools: process.env.DEVTOOLS === 'true',

    browser: 'chromium',
    args: [
      // '--start-fullscreen',
      '--start-maximized',
      '--no-sandbox',
      '--disable-gpu',
      '--disable-infobars',
      '--disable-dev-shm-usage',
      '--disable-accelerated-2d-canvas',

      '--disable-background-networking',
      '--disable-background-timer-throttling',
      '--disable-client-side-phishing-detection',
      '--disable-default-apps',
      '--disable-extensions',
      '--disable-hang-monitor',
      '--disable-popup-blocking',
      '--disable-prompt-on-repost',
      '--disable-sync',
      '--disable-translate',
    ],
  },
  defaultViewport: null,
  browserContext: 'incognito',
  // browserContext: process.env.INCOGNITO === 'true' ? 'incognito' : 'default',
  exitOnPageError: false,
};
