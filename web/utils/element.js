import * as globalSetup from '../../config/global.setup';
import info from '../../utils/loggers';

export default class Element {
  constructor(selector) {
    this.selector = selector;
  }

  newChildElement(childSelector) {
    return new Element(`${this.selector} ${childSelector}`);
  }

  async wait(timeout = globalSetup.longTimeout) {
    info(`Waiting for ${this.selector} ...`);
    const elementHandle = await page.waitForSelector(this.selector, {
      timeout,
    });
    return elementHandle;
  }

  async waitUntilVisible(timeout = globalSetup.longTimeout) {
    info(`Waiting for ${this.selector} to be visible...`);
    const elementHandle = await page.waitForSelector(this.selector, {
      visible: true,
      timeout,
    });
    return elementHandle;
  }

  async waitUntilInvisible(timeout = globalSetup.longTimeout) {
    info(`Waiting for ${this.selector} to be invisible...`);
    await page.waitForSelector(this.selector, { hidden: true, timeout });
  }

  async waitUntilDisabled(timeout = globalSetup.longTimeout) {
    info(`Waiting for ${this.selector} to be disabled...`);
    await page.waitForSelector(`${this.selector}:disabled`, { timeout });
  }

  async waitUntilEnabled(timeout = globalSetup.longTimeout) {
    info(`Waiting for ${this.selector} to be disabled...`);
    await page.waitForSelector(`${this.selector}:not([disabled])`, { timeout });
  }

  async waitForTimeout(timeout = globalSetup.defaultTimeout) {
    info(`Waiting ${timeout} ms...`);
    await page.waitForTimeout(timeout);
  }

  async click() {
    info(`Clicking ${this.selector} ...`);
    const elementHandle = await this.wait();
    await elementHandle.click();
  }

  async exists(timeout = globalSetup.veryShortTimeout) {
    let exist;
    try {
      await this.wait(timeout);
      exist = true;
      info(`Element ${this.selector} exists`);
    } catch (error) {
      exist = false;
      info(`Element ${this.selector} does not exist`);
    }
    return exist;
  }

  async isVisible(timeout) {
    let visible;
    try {
      await this.waitUntilVisible(timeout);
      visible = true;
      info(`Element ${this.selector} is visible`);
    } catch (error) {
      visible = false;
      info(`Element ${this.selector} is not visible`);
    }
    return visible;
  }

  async isDisabled() {
    const isDisabled = await page.$eval(
      this.selector,
      (button) => button.disabled,
    );
    if (isDisabled) {
      info(`Element ${this.selector} is disabled`);
    } else {
      info(`Element ${this.selector} is enabled`);
    }
    return isDisabled;
  }

  async isActive() {
    const isActive = await page.$eval(
      this.selector,
      (button) => button.classList[1] === 'active',
    );
    if (isActive) {
      info(`Element ${this.selector} is activce`);
    } else {
      info(`Element ${this.selector} is not active`);
    }
    return isActive;
  }

  async text() {
    info(`Getting inner text of ${this.selector} ...`);
    const elementHandle = await this.wait();
    const text = await elementHandle.evaluate((element) => element.innerText);
    return text;
  }

  async value() {
    info(`Getting value of ${this.selector} ...`);
    const elementHandle = await this.wait();
    const value = await elementHandle.evaluate((element) => element.value);
    return value;
  }

  async enterText(text, withDelay = false) {
    info(`Entering the text value ${text} for ${this.selector} ...`);
    const elementHandle = await this.wait();
    await elementHandle.type(text, withDelay ? { delay: globalSetup.shortTimeout } : {});
  }

  async pressKeyboardKey(key) {
    info('Pressing keyboard button ...');
    await page.keyboard.press(key);
  }

  async tripleClick() {
    info(`Triple clicking ${this.selector} ...`);
    const elementHandle = await this.wait();
    await elementHandle.click({ clickCount: 3 });
  }

  async clearAndEnterText(text) {
    await this.tripleClick();
    await this.pressKeyboardKey('Backspace');
    await this.enterText(text);
  }

  async getAttributeValue(attributeName) {
    const elementHandle = await this.wait();
    info(
      `Getting '${attributeName}' attribute value of element ${this.selector} ...`,
    );
    const attributeValue = await elementHandle.evaluate(
      (element, attributeName) => element.getAttribute(`${attributeName}`),
      attributeName,
    );
    return attributeValue;
  }

  async getStyleAttributeValue(name) {
    const elementHandle = await this.wait();
    info(`Getting value of style attribute '${name}' of ${this.selector} ...`);
    const attributeValue = await elementHandle.evaluate(
      (element, name) => element.style[name],
      name,
    );
    return attributeValue;
  }

  async hover() {
    info(`Hovering mouse on to ${this.selector} ...`);
    const elementHandle = await this.wait();
    await elementHandle.hover();
  }

  async getAllElements() {
    info(`Getting all elements with ${this.selector} selector ...`);
    await this.wait();
    const elements = await page.$$(this.selector);
    return elements;
  }

  async getNumberOfElements() {
    info(`Getting number of all elements with ${this.selector} selector ...`);
    const count = await this.getAllElements();
    return count.length;
  }

  async getAllElementsText() {
    await this.wait();
    const allText = await page.$$eval(this.selector, (elements) => elements.map((item) => item.textContent));
    return allText;
  }
}
