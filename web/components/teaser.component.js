import Element from '../utils/element';

export default class TeaserComponent extends Element {
  constructor() {
    super('article.bt-teaser');

    this.button = {
      favorite: this.newChildElement('.bt-button'),
      markAsFavorite: (isOn = false) => this.newChildElement(`.bt-favorite-icon--${isOn ? 'on' : 'off'}`),
    };

    this.toggle = {
      mapView: this.newChildElement('#bt-show-map'),
    };
  }
}
