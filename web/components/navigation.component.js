import Element from '../utils/element';

export default class HeaderComponent extends Element {
  constructor() {
    super('.mainNavigation');

    this.link = {
      vacationRentals: this.newChildElement('[href="/hub-test-vacation-rentals"]'),
      hotTubProperties: this.newChildElement('[href="/hot-tub-properties"]'),
    };
  }
}
