import Element from '../utils/element';

export default class QuantityCounterComponent extends Element {
  constructor(selector = '.bt-range-filter-wrapper') {
    super(selector);

    this.filter = this.newChildElement('.bt-range-filter');

    this.label = {
      name: this.newChildElement('.bt-range-filter__label'),
      value: this.newChildElement('.bt-range-filter__value'),
    };

    this.button = {
      decrease: this.newChildElement('.bt-range-filter__button--minus'),
      increase: this.newChildElement('.bt-range-filter__button--plus'),
    };
  }

  async getQuantityCounterByName(quantityCounterName) {
    const quantityCounters = await this.filter.getAllElementsText();
    const index = quantityCounters.findIndex((name) => name.includes(quantityCounterName));
    return new QuantityCounterComponent(`${this.selector}:nth-child(${index + 1})`);
  }

  async getQuantity() {
    const value = await this.label.value.text();
    return value;
  }

  async increase() {
    await this.button.increase.click();
    await this.button.increase.waitUntilDisabled();
    await this.button.increase.waitUntilEnabled();
  }

  async decrease() {
    await this.button.decrease.click();
    await this.button.decrease.waitUntilDisabled();
    await this.button.decrease.waitUntilEnabled();
  }
}
