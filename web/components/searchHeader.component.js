import Element from '../utils/element';

export default class HeaderComponent extends Element {
  constructor() {
    super('.bt-search__header');

    this.button = {
      favoritiesCounter: this.newChildElement('.bt-favorites-mode-toggle'),
      filters: this.newChildElement('.bt-modal-toggle--filters'),
    };

    this.toggle = {
      mapView: this.newChildElement('#bt-show-map'),
    };
  }
}
