import Element from '../utils/element';

export default class SearchResultHeaderComponent extends Element {
  constructor() {
    super('.bt-results-list__header');

    this.label = {
      resultCount: this.newChildElement('.bt-result-count'),
      noResults: this.newChildElement('.bt-no-results--favorites'),
    };

    this.dropdown = {
      sorts: this.newChildElement('.bt-sorts'),
    };
  }
}
