import Element from '../utils/element';
import QuantityCounterComponent from './quantityCounter.component';

export default class HeaderComponent extends Element {
  constructor() {
    super('[aria-label="Filter Results"]');

    this.counter = new QuantityCounterComponent();

    this.label = {
      tittle: this.newChildElement('.bt-modal__heading'),
      resultCount: this.newChildElement('.bt-result-count'),
      minimumBedrooms: this.newChildElement('span[id$="Bedrooms"]'),
      minimumBathrooms: this.newChildElement('span[id$="Bathrooms"]'),
    };

    this.button = {
      viewResults: this.newChildElement('.bt-button--cta'),
      clearFilters: this.newChildElement('button.bt-clear-filters'),
    };
  }
}
