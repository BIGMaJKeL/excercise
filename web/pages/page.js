import { baseUrl, longTimeout } from '../../config/global.config';
import loggers from '../../utils/loggers';
import PageElements from './page.elements';
import CookiesType from '../../constant/cookies';
import Viewport from '../../constant/viewport';

const logger = loggers(global.logFileName);
export default class Page extends PageElements {
  constructor() {
    super();
    this.baseUrl = baseUrl;
    this.path = '/';
  }

  async visit(path = this.path, navigationType = 'networkidle0') {
    const fullUrl = `${this.baseUrl}${path}`;
    logger.info(`Visiting page with URL: ${fullUrl}`);
    await page.goto(fullUrl, {
      timeout: 3 * longTimeout,
      waitUntil: navigationType,
    });
  }

  async visitPage({
    path = this.path,
    navigationType = 'networkidle0',
    shouldSetCookies = true,
  }) {
    const fullUrl = `${this.baseUrl}${path}`;
    logger.info(`Visiting page with URL: ${fullUrl}`);
    if (shouldSetCookies === true) await this.setCookies();
    await page.goto(fullUrl, {
      timeout: 3 * longTimeout,
      waitUntil: navigationType,
    });
  }

  async setCookies(cookieObject = CookiesType.bluetentConsent) {
    await page.setCookie({
        ...cookieObject,
        domain: this.baseUrl.split('//').splice(-1)[0],
    });
  }

  async deleteCookies(cookieObject = CookiesType.bluetentConsent) {
    await page.deleteCookie(cookieObject);
  }

  async setViewport(viewport = Viewport.desktop) {
    logger.info(
      `Setting the papge view port: ${viewport.width} x ${viewport.height}`,
    );
    await page.setViewport(viewport);
  }

  setViewportOn = {
    desktop: async () => {
      await this.setViewport(Viewport.desktop);
    },
    mobile: async () => {
      await this.setViewport(Viewport.mobile);
    },
  };

  async takeFullPageScreenshot() {
    logger.info(`Taking a full page screenshot...`);
    const image = await page.screenshot({ fullPage: true });
    return image;
  }

  async takeScreenshot() {
    logger.info(`Taking a  screenshot...`);
    const image = await page.screenshot({ fullPage: false });
    return image;
  }

  async waitUntilNavigation(navigationType = 'networkidle0') {
    await page.waitForNavigation({
      timeout: 3 * longTimeout,
      waitUntil: navigationType,
    }); //networkidle0, domcontentloaded
  }

  async waitForUrl(path = this.path, timeoutForUrl = longTimeout) {
    const fullUrl = `${this.baseUrl}${path}`;
    logger.info(`Waiting for URL ${fullUrl} to be achived...`);
    await page.waitForFunction(`document.URL === '${fullUrl}'`, {
      timeout: timeoutForUrl,
    });
  }

  async waitForUrlContains(path = this.path, timeoutForUrl = longTimeout) {
    const fullUrl = `${this.baseUrl}${path}`;
    logger.info(`Waiting for URL contain ${fullUrl} to be achived...`);
    await page.waitForFunction(`document.URL.includes('${fullUrl}')`, {
      timeout: timeoutForUrl,
    });
  }

  async refresh(timeoutForUrl = longTimeout) {
    await page.reload({ timeout: timeoutForUrl });
  }

  async getTitle() {
    return await page.title();
  }

  async getUrl() {
    return page.url();
  }
}
