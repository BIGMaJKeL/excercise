import Page from '../page';
import Element from '../../utils/element';
import NavigationComponent from '../../components/navigation.component';

class MainPage extends Page {
  constructor() {
    super();
    this.title = 'The future of online booking';

    this.component = {
      navigation: new NavigationComponent(),
    };

    this.button = {
      bookNow: new Element('[href="/vacation-rentals"]'),
    };
  }
}

export default new MainPage();
