// SHOP
import main from './main/main.page';
import vacationRentals from './vacationRentals/vacationRentals.page';
import teaserDetails from './teaserDetails/teaserDetails.page';

const PAGE = {
  main,
  vacationRentals,
  teaserDetails,
};

export default PAGE;
