import Page from '../page';
import SearchHeaderComponent from '../../components/searchHeader.component';
import TeaserComponent from '../../components/teaser.component';
import SearchResultHeaderComponent from '../../components/searchResultHeaderContent.component';
import FilterResultsComponent from '../../components/filterResults.component';
import Element from '../../utils/element';

class VacationRentalsPage extends Page {
  constructor() {
    super();
    super.path = '/hub-test-vacation-rentals';
    this.title = 'Main Search Interface — The future of online booking';

    this.loading = new Element('.bt-teaser-wrapper__loading');

    this.component = {
      searchHeader: new SearchHeaderComponent(),
      teaser: new TeaserComponent(),
      searchResultHeader: new SearchResultHeaderComponent(),
      filterResults: new FilterResultsComponent(),
    };
  }

  async waitUntilLoading() {
    await this.loading.waitUntilVisible();
    await this.loading.waitUntilInvisible();
  }
}

export default new VacationRentalsPage();
