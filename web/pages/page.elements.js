export default class PageElements {
  constructor() {
    this.button = {};
    this.field = {};
    this.checkbox = {};
    this.toggle = {};
    this.link = {};
    this.dropdown = {};
    this.label = {};
    this.message = {};
  }
}
