import Page from '../page';
import Element from '../../utils/element';
import SearchHeaderComponent from '../../components/searchHeader.component';

class TeaserDetailsPage extends Page {
  constructor() {
    super();
    super.path = '/hub-test-details';
    this.title = 'Details Page — The future of online booking';

    this.component = {
      searchHeader: new SearchHeaderComponent(),
    };

    this.button = {
      markAsFavorite: (isOn = false) => new Element(`.bt-favorite--${isOn ? 'flagged' : 'unflagged'}`),
    };
  }
}

export default new TeaserDetailsPage();
