import PAGE from '../../pages/list.pages';
/*
* As a user I want the ability to filter properties based on the number of bedrooms and bathrooms.
*
* **Acceptance Criteria:**
*
* The Filters selection should allow the user to select the number of either bedrooms and/or bathrooms.
* The selection should limit the value to an integer with a lower value of 0 (zero).
* The Clear Filters button should reset both filters to their lower value.
* The View Results button should close the Filter Results page and display properties on the hub meeting the criteria.
*
* Story Artifacts:
*
* Main Hub is at https://www.rezfusionhubdemo.com/hub-test-vacation-vacationRentals
*/
describe('As a newcomer', () => {
  describe('when I`m on vacation rentals page', () => {
    beforeAll(async () => {
      await PAGE.vacationRentals.visitPage({ shouldSetCookies: true });
      await PAGE.vacationRentals.waitForUrl();
    });

    describe('I should be able to filter teasers', () => {
      beforeEach(async () => {
        await PAGE.vacationRentals.component.searchHeader.button.filters.click();
        await PAGE.vacationRentals.component.filterResults.waitUntilVisible();
      });

      describe('I should see filter modal details', () => {
        it('and see modal title: FILTER RESULTS', async () => {
          expect(await PAGE.vacationRentals.component.filterResults.label.tittle.text()).toBe('FILTER RESULTS');
        });

        it('and see that minimum bedooms is set by default to 0', async () => {
          expect(await PAGE.vacationRentals.component.filterResults.label.minimumBedrooms.text()).toBe('0');
        });

        it('and see that minimum bathrooms is set by default to 0', async () => {
          expect(await PAGE.vacationRentals.component.filterResults.label.minimumBathrooms.text()).toBe('0');
        });
      });

      describe('by selecting the number of either bedrooms and/or bathrooms', () => {
        it('after click on increasing and deacrising the minimum Bedrooms', async () => {
          const bedrooms = await PAGE.vacationRentals.component.filterResults.counter.getQuantityCounterByName('Minimum Bedrooms');
          expect(await bedrooms.label.name.text()).toBe('Minimum Bedrooms');

          await bedrooms.increase();
          expect(await bedrooms.getQuantity()).toBe('1');

          await bedrooms.increase();
          expect(await bedrooms.getQuantity()).toBe('2');

          await bedrooms.increase();
          expect(await bedrooms.getQuantity()).toBe('3');

          await bedrooms.increase();
          expect(await bedrooms.getQuantity()).toBe('4');

          await bedrooms.increase();
          expect(await bedrooms.getQuantity()).toBe('5');

          await bedrooms.decrease();
          expect(await bedrooms.getQuantity()).toBe('4');

          await bedrooms.decrease();
          expect(await bedrooms.getQuantity()).toBe('3');

          await bedrooms.decrease();
          expect(await bedrooms.getQuantity()).toBe('2');

          await bedrooms.decrease();
          expect(await bedrooms.getQuantity()).toBe('1');

          await bedrooms.decrease();
          expect(await bedrooms.getQuantity()).toBe('0');

          //The selection should limit the value to an integer with a lower value of 0 (zero).
          await bedrooms.decrease();
          expect(await bedrooms.getQuantity()).toBe('0');
        });

        it('after click on increasing and deacrising the minimum Bathrooms', async () => {
          const bathrooms = await PAGE.vacationRentals.component.filterResults.counter.getQuantityCounterByName('Minimum Bathrooms');
          expect(await bathrooms.label.name.text()).toBe('Minimum Bathrooms');

          await bathrooms.increase();
          expect(await bathrooms.getQuantity()).toBe('1');

          await bathrooms.increase();
          expect(await bathrooms.getQuantity()).toBe('2');

          await bathrooms.increase();
          expect(await bathrooms.getQuantity()).toBe('3');

          await bathrooms.increase();
          expect(await bathrooms.getQuantity()).toBe('4');

          await bathrooms.increase();
          expect(await bathrooms.getQuantity()).toBe('5');

          await bathrooms.decrease();
          expect(await bathrooms.getQuantity()).toBe('4');

          await bathrooms.decrease();
          expect(await bathrooms.getQuantity()).toBe('3');

          await bathrooms.decrease();
          expect(await bathrooms.getQuantity()).toBe('2');

          await bathrooms.decrease();
          expect(await bathrooms.getQuantity()).toBe('1');

          await bathrooms.decrease();
          expect(await bathrooms.getQuantity()).toBe('0');

          //The selection should limit the value to an integer with a lower value of 0 (zero).
          await bathrooms.decrease();
          expect(await bathrooms.getQuantity()).toBe('0');
        });

        it('after click on Clear Filters button should reset filters to their lower value', async () => {
          const bedrooms = await PAGE.vacationRentals.component.filterResults.counter.getQuantityCounterByName('Minimum Bedrooms');
          expect(await bedrooms.label.name.text()).toBe('Minimum Bedrooms');

          await bedrooms.increase();
          await bedrooms.increase();
          expect(await bedrooms.getQuantity()).toBe('2');

          const bathrooms = await PAGE.vacationRentals.component.filterResults.counter.getQuantityCounterByName('Minimum Bathrooms');
          expect(await bathrooms.label.name.text()).toBe('Minimum Bathrooms');

          await bathrooms.increase();
          await bathrooms.increase();
          expect(await bathrooms.getQuantity()).toBe('2');

          expect(await PAGE.vacationRentals.component.filterResults.button.clearFilters.text()).toBe('Clear Filters');
          await PAGE.vacationRentals.component.filterResults.button.clearFilters.click();

          expect(await bedrooms.getQuantity()).toBe('0');
          expect(await bathrooms.getQuantity()).toBe('0');
        });

        it('the View Results button should close the Filter Results page and display properties on the hub meeting the criteria', async () => {
          const bedrooms = await PAGE.vacationRentals.component.filterResults.counter.getQuantityCounterByName('Minimum Bedrooms');
          await bedrooms.increase();
          await bedrooms.increase();
          await bedrooms.increase();

          const bathrooms = await PAGE.vacationRentals.component.filterResults.counter.getQuantityCounterByName('Minimum Bathrooms');
          await bathrooms.increase();
          await bathrooms.increase();
          await bathrooms.increase();

          const resultsCount = await PAGE.vacationRentals.component.filterResults.label.resultCount.text();
          await PAGE.vacationRentals.component.filterResults.button.viewResults.click();
          await PAGE.vacationRentals.component.filterResults.waitUntilInvisible();

          expect(await PAGE.vacationRentals.component.searchResultHeader.label.resultCount.text()).toBe(resultsCount);
        });
      });
    });
  });
});

