import PAGE from '../../pages/list.pages';

/**
 * As a user I want the ability to save properties I’m looking at so they can be reviewed at a later time.
 * 
 * Acceptance Criteria:
 * 
 * Each property on the hub should have an easy Favorite on/off button indicating that this property has been saved.
 * The hub should have an indicator on the number of properties selected as saved.
 * This indicator should show the total count of saved properties.
 * When the indicator is clicked the hub should only display saved properties.
 * I can un-save a property from the filtered view.
 * When a property is selected; there should be an indicator showing the property has been saved.
 * This indicator can be toggled on or off from the property detail’s view
 * This change (saved/un-saved) should reflect correctly on the total saved count on the main hub
 * 
 * Story Artifacts:
 * Main Hub is at https://www.rezfusionhubdemo.com/hub-test-vacation-rentals
 */
describe('As a newcomer', () => {
  describe('when I`m on vacation rentals page', () => {
    beforeAll(async () => {
      await PAGE.vacationRentals.visit();
      await PAGE.vacationRentals.waitForUrl();
    });

    describe('I should be able mark teaser as my favorite from teaser details', () => {
      beforeAll(async () => {
        expect(await PAGE.vacationRentals.component.teaser.getNumberOfElements()).toBeGreaterThanOrEqual(1);
        await PAGE.vacationRentals.component.teaser.click();
        await PAGE.teaserDetails.waitForUrlContains();
      });

      it('and the favorive icon should be marked as unflaged by default', async () => {
        expect(await PAGE.teaserDetails.button.markAsFavorite().isVisible()).toBeTruthy();
      });

      it('and the favotive button should has text Save by default', async () => {
        expect(await PAGE.teaserDetails.button.markAsFavorite().text()).toBe('Save');
      });

      it('after click on the favotive button I should see that text is changed to Saved', async () => {
        await PAGE.teaserDetails.button.markAsFavorite().click();
        expect(await PAGE.teaserDetails.button.markAsFavorite(true).text()).toBe('Saved');
      });
    });
  });
});
