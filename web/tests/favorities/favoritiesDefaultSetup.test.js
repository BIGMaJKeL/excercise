import PAGE from '../../pages/list.pages';

describe('As a newcomer', () => {
  describe('when I`m on vacation rentals page', () => {
    beforeAll(async () => {
      await PAGE.vacationRentals.visit();
      await PAGE.vacationRentals.waitForUrl();
    });

    describe('I should see search header', () => {
      beforeAll(async () => {
        await PAGE.vacationRentals.component.searchHeader.waitUntilVisible();
      });

      it('and see toggle [MapView]', async () => {
        await PAGE.vacationRentals.component.searchHeader.toggle.mapView.waitUntilVisible();
      });

      it('and see button [favorite]', async () => {
        await PAGE.vacationRentals.component.searchHeader.button.favoritiesCounter.waitUntilVisible();
      });

      it('and the favorite counter should set 0 as default', async () => {
        expect(await PAGE.vacationRentals.component.searchHeader.button.favoritiesCounter.text()).toBe('(0)');
      });
    });

    describe('I should at least one teaser', () => {
      beforeAll(async () => {
        expect(await PAGE.vacationRentals.component.teaser.getNumberOfElements()).toBeGreaterThanOrEqual(1);
      });

      it('the favorive icon should be marked as off', async () => {
        expect(await PAGE.vacationRentals.component.teaser.button.markAsFavorite(false).isVisible()).toBeTruthy();
      });
    });
  });
});
