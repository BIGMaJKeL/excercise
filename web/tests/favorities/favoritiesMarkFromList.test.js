import PAGE from '../../pages/list.pages';

/**
 * As a user I want the ability to save properties I’m looking at so they can be reviewed at a later time.
 * 
 * Acceptance Criteria:
 * 
 * Each property on the hub should have an easy Favorite on/off button indicating that this property has been saved.
 * The hub should have an indicator on the number of properties selected as saved.
 * This indicator should show the total count of saved properties.
 * When the indicator is clicked the hub should only display saved properties.
 * I can un-save a property from the filtered view.
 * When a property is selected; there should be an indicator showing the property has been saved.
 * This indicator can be toggled on or off from the property detail’s view
 * This change (saved/un-saved) should reflect correctly on the total saved count on the main hub
 * 
 * Story Artifacts:
 * Main Hub is at https://www.rezfusionhubdemo.com/hub-test-vacation-rentals
 */
describe('As a newcomer', () => {
  describe('when I`m on vacation rentals page', () => {
    beforeAll(async () => {
      await PAGE.vacationRentals.visit();
      await PAGE.vacationRentals.waitForUrl();
    });

    //TODO: Please keep in mind that tests are sensitive - it is demo purpose only
    describe('I should be able mark teaser as my favorite from list', () => {
      beforeAll(async () => {
        expect(await PAGE.vacationRentals.component.teaser.getNumberOfElements()).toBeGreaterThanOrEqual(1);
        await PAGE.vacationRentals.component.teaser.button.markAsFavorite(false).click();
      });

      it('and the favorive icon should be marked as on', async () => {
        expect(await PAGE.vacationRentals.component.teaser.button.markAsFavorite(true).isVisible()).toBeTruthy();
      });

      it('and the favorite counter should set 1', async () => {
        expect(await PAGE.vacationRentals.component.searchHeader.button.favoritiesCounter.text()).toBe('(1)');
      });

      it('and the results counter is set to 1 after click on favorites', async () => {
        await PAGE.vacationRentals.component.searchHeader.button.favoritiesCounter.click();
        await PAGE.vacationRentals.waitUntilLoading();

        expect(await PAGE.vacationRentals.component.searchResultHeader.label.resultCount.text()).toBe('1 Results');
      });

      it('and the favorite counter is saved and visible after refresh the page', async () => {
        await PAGE.vacationRentals.refresh();
        expect(await PAGE.vacationRentals.component.searchHeader.button.favoritiesCounter.text()).toBe('(1)');
      });

      it('and I should be able to unmark the favorite and counter should be set to 0', async () => {
        await PAGE.vacationRentals.component.teaser.button.markAsFavorite(true).click();
        expect(await PAGE.vacationRentals.component.searchHeader.button.favoritiesCounter.text()).toBe('(0)');
      });

      it('and the favorite counter is saved and visible after refresh the page', async () => {
        await PAGE.vacationRentals.refresh();
        expect(await PAGE.vacationRentals.component.searchHeader.button.favoritiesCounter.text()).toBe('(0)');
      });
    });
  });
});
