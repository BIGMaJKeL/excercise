const { defineConfig } = require('cypress');

module.exports = defineConfig({
  projectId: 'iusvvu',
  e2e: {
    baseUrl: 'https://www.rezfusionhubdemo.com',
    specPattern: 'cypress/e2e/**/*.cy.{js,jsx,ts,tsx}',
  },
});
