module.exports = {
  env: {
    jest: true,
    browser: true,
    es2020: true,
  },
  globals: {
    app: true,
    page: true,
    browser: true,
    context: true,
    jestPuppeteer: true,
    'cypress/globals': true,
  },
  plugins: ['cypress'],
  extends: ['airbnb-base', 'plugin:cypress/recommended'],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  rules: {
    'no-plusplus': ['error', { allowForLoopAfterthoughts: true }],
    'max-len': ['error', { code: 250 }],
    'cypress/no-assigning-return-values': 'error',
    'cypress/no-unnecessary-waiting': 'error',
    'cypress/assertion-before-screenshot': 'warn',
    'cypress/no-force': 'warn',
    'cypress/no-async-tests': 'error',
    'cypress/no-pause': 'error',
  },
  overrides: [],
};
